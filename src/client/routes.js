import customLoadable from './components/customLoadable';
import React from 'react';

const routes =  [
    {
        path: '/',
        exact: true,
        component: customLoadable({
            loader: () => import('./components/ContentPane')
        }),
    },
    {
        path: '/contacts',
        exact: true,
        component: customLoadable({
            loader: () => import('./components/Contacts')
        })
    },
    // {
    //     path: '/about',
    //     component: customLoadable({
    //         loader: () => import('./components/RepoGrid')
    //     }),
    //
    // },
    // {
    //     path: '/users',
    //     exact: true,
    //     component: customLoadable({
    //         loader: () => import('./components/Users')
    //     })
    // },
    // {
    //     path: '/users/:name',
    //     component: customLoadable({
    //         loader: () => import('./components/UserGrid')
    //     }),
    //     loadData: (store, match) => {
    //         import('./redux-modules/usersModule')
    //             .then( module => {
    //                 return store.dispatch( module.default( match.params.language ) )
    //             })
    //             .catch( err => console.log(err) )
    //     }
    // },
    // {
    //     path: '/404',
    //     exact: true,
    //     component: customLoadable({
    //         loader: () => import('./components/PageNotFound')
    //     })
    // },
    // {
    //     path: "/",
    //     component: () => <Redirect to="/404" />
    // }
];

export default routes;