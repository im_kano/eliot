import { combineReducers } from 'redux';
import gif from '../modules/gif';

const reducers = {
    gif,
};
export default combineReducers(reducers);