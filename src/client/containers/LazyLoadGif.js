import React from 'react';

export default class LazyLoadGif extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            currentUrl: this.props.url,
            loaded: false,
            loading: false,
        }
    };
    loadImage(url){
        let gif = new Image();
        let data = new Date();
        gif.onload = () => {
            let curDate = new Date();
            let timeDiff = curDate.getTime() - data.getTime();
            setTimeout(() => {
                this.setState({
                    loaded: true,
                    loading: false,
                    currentUrl: url
                });
            }, timeDiff < 1000 ? 1000 - timeDiff : 0)
        };
        this.setState({
            loaded: false,
            loading: true
        });
        gif.src = url;
    }
    componentDidMount() {
        this.loadImage(this.props.url);
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.url !== this.props.url) {
            this.loadImage(nextProps.url);
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        if(this.state.currentUrl !== nextState.currentUrl) {
            return true;
        }
        if(this.state.loading !== nextState.loading) {
            return true;
        }
        return false;
    }
    render() {

        return (
            <div className='loading-anime'>
                <div className='exact-gif'>
                    <img src={this.state.currentUrl} style={this.state.loading ? {filter: "brightness(50%)"} : null}  alt='gif' />
                </div>
                <div className={this.state.loading ? "lds-dual-ring": ''}>
                </div>
            </div>
    )
    }
}