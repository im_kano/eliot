import React, {Component} from 'react';
import {Switch, Route, BrowserRouter} from 'react-router-dom';
import {connect} from 'react-redux';

import {gifAction} from '../modules/gif';
import LeftPane from '../components/LeftPane';
import RightPane from '../components/RightPane';
import ContentPane from '../components/ContentPane';
import Contacts from '../components/Contacts';
import Learn from '../components/Learn';
import BuyPage from "../components/BuyPage";
import RegLoginModal from "../components/RegLogin";
import '../scss/EliotApp.scss';
// import {renderRoutes} from "react-router-config";
// import routes from '../routes';

class EliotApp extends Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollState: false,
            loaded: false,
            showModal: false,
            showLogRegModal: false,
            isLoggedIn: false,
        };
    }

    checkScrolling = e => {
        e.preventDefault();
        this.setState({
            scrollState: (window.scrollY > 100)
        })
    };

    escFunction = e => {
        if (e.keyCode === 27 && this.state.showModal) {
            this.showModalHandler();
        }else if (e.keyCode === 27 && this.state.showLogRegModal){
            this.showRegLogHandler();
        }
    };

    componentDidMount() {
        window.addEventListener('scroll', this.checkScrolling);
        window.addEventListener('keydown', this.escFunction, false);
    };

    componentWillUnmount() {
        window.removeEventListener('scroll', this.checkScrolling);
        window.removeEventListener('keydown', this.escFunction, false);
    };


    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.scrollState !== nextState.scrollState || this.state.showModal !== nextState.showModal || this.state.showLogRegModal !== nextState.showLogRegModal) {
            document.body.style.overflow = nextState.showModal ? "hidden" : "auto";
            return true;
        }
        return this.props.featureObj !== nextProps.featureObj;
    };

    showRegLogHandler = () => {
        const { showLogRegModal } = this.state;
        this.setState({
            showLogRegModal: !showLogRegModal,
        }, () => console.log(this.state.showLogRegModal));
    };

    showModalHandler = () => {
        const {showModal} = this.state;
        this.setState({
            showModal: !showModal,
        });
    };


    openStore = () => {
        const url = 'https://assetstore.unity.com/packages/tools/ai/eliot-ai-framework-128119';
        window.open(url, '_blank');
        this.showModalHandler();
    };

    handleSubmit = e => {
        e.preventDefault();
        let emailObj = {
            name: e.target.name.value,
            email: e.target.email.value,
            subject: e.target.subject.value,
            message: e.target.message.value,
        };
    };

    render() {
        return (
            <BrowserRouter>
                <div className={this.state.scrollState ? 'eliot-app scrolled' : 'eliot-app'}>
                    {this.state.showModal && <BuyPage openStore={this.openStore}/>}
                    <LeftPane showModal={this.state.showModal} showModalHandler={this.showModalHandler}/>
                    <Switch>
                        <Route
                            exact path='/'
                            render={
                                () => <ContentPane
                                    activeFeature={this.props.activeFeature}
                                    loadSingleGif={this.props.loadSingleGif}
                                    featureObj={this.props.featureObj}
                                />
                            }
                        />
                        <Route
                            path='/contacts'
                            component={Contacts}
                        />
                        <Route
                            path='/learn'
                            component={Learn}
                        />
                    </Switch>
                    {this.state.showLogRegModal && <RegLoginModal isLoggedIn = {this.state.isLoggedIn} />}
                    <RightPane
                        showRegLogHandler={this.showRegLogHandler}
                    />
                </div>
            </BrowserRouter>
        );
    };
}

const mapDispatchToProps = dispatch => ({
    loadSingleGif: featureObj => dispatch(gifAction(featureObj)),
    openStore: () => this.openStore(),
});

const mapStateToProps = state => ({
    featureObj: state.gif.featureObj,
    activeFeature: state.gif.active,
});

export default connect(mapStateToProps, mapDispatchToProps)(EliotApp);