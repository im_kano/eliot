import reducerRegistry from './reducerRegistry';

export const initialState = {
    featureObj: {
        name: 'VISUAL PROGRAMMING',
        descr: 'Build complex behaviours within minutes.'
    },
    active: 'VISUAL PROGRAMMING',
};


export const createActionName = name => `/${name}`;

export const LOAD_GIF = createActionName('LOAD_GIF');


//action creators
export const gifAction = payload => ({ type: LOAD_GIF , payload });



export const reducerName = "gif";

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case LOAD_GIF:
            return {
                ...state,
                featureObj: action.payload,
                active: action.payload.name,
            };
        default:
            return state;
    }
};



//selectors
// export const selectFeatureObj = state => state.singleFeature;
// export const selectActiveFeature = state => state.active;



reducerRegistry.register(reducerName, reducer);