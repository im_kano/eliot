import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import EliotApp from './containers/EliotApp';
import store from './configureStore';

ReactDOM.render(
    <Provider store={store}>
        <EliotApp/>
    </Provider>
    , document.getElementById('app'));
