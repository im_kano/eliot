import React from 'react';

import githubLogo from '../../../static/img/github.png';
import googleplusLogo from '../../../static/img/googleplus.png';
import facebookLogo from '../../../static/img/facebooklogin.png';
import '../scss/RegLogin.scss';

const RegLoginModal = ({isLoggedIn}) => {
    const login = <div className='login-wrapper'>
        <div className='login-wrapper-icons'>
            <img src={githubLogo} alt="github"/>
            <img src={googleplusLogo} alt="googleplus"/>
            <img src={facebookLogo} alt="facebook"/>
        </div>
        <div className='login-wrapper-delimiter'>
            <span> </span>
            <h1>or</h1>
            <span> </span>
        </div>
        <div className='login-wrapper-inputs'>
            <input type="email" placeholder='email'/>
            <input type="password" placeholder='password'/>
            <button>LOGIN</button>
        </div>
        <div className="login-wrapper-bottom">
            <h1>looking to <span>create an account?</span></h1>
        </div>
    </div>;

    const logout = <div className='logout-wrapper'>

    </div>;

    return(
    <div className='reg-log-wrapper'>
        <div className='reg-log-wrapper-header'>
            <h1>{isLoggedIn ? 'something' : 'login with'}</h1>
        </div>
        {isLoggedIn ? logout : login}
    </div>
    );
};
export default RegLoginModal;