import React from 'react';
import '../scss/BuyPage.scss';

const BuyPage = ({openStore}) => (
    <div className="buy-page-modal">
        <div className="buy-page-modal-back">
            <div className="buy-page-modal-back-top-text">
                <h1>
                    Full License
                    <span>Unity Asset Store</span>
                </h1>
            </div>

            <div className="buy-page-modal-back-bottom-text">
                <div className="buy-page-modal-back-bottom-text-list">
                    <ul>
                        <li>Commercial use</li>
                        <li>Free updates</li>
                        <li>Manual & Tutorials</li>
                        <li>Technical support</li>
                        <li>Example project</li>
                    </ul>
                    <h1>50$</h1>
                </div>
            </div>
            <div className="buy-page-modal-back-bottom-text-button">
                <button onClick={openStore}>
                    BUY
                </button>
            </div>
        </div>
    </div>
);
export default BuyPage;
