import React from 'react';

import '../scss/Contacts.scss';

const Contacts = ({handleSubmit}) => (
    <div className="contacts">
        <div className="contacts-text">
            <h1>Have any questions</h1>
            <p>
                I'll be happy to answer any questions, to receive your critique and propositions. You can use the form
                here, write an email (to eliotaiproject@gmail.com) or use social networks. You can follow news about
                Eliot on Facebook, Twitter, Instagram and LinkedIn.
                Feel free to reach me out in case you have any product related questions, propositions, ideas etc.
            </p>
        </div>

        <div className="contacts-form">
            <form onSubmit={handleSubmit}>
                <input name="name" type="text" placeholder="name"/>
                <input name="email" type="text" placeholder="email"/>
                <input name="subject" type="text" placeholder="subject"/>
                <textarea name="message" placeholder="write something">
          {null}
        </textarea>
                <button type="submit">
                    SEND EMAIL
                </button>
            </form>
        </div>
    </div>
);
export default Contacts;
