import React from 'react';

import '../scss/MoreFeatures.scss';

const MoreFeatures = props => (
    <div className='more-features'>
        <div className='more-features-content'>
            <div className='more-features-column-item'>
                <h1>Dynamic 'smartness' of AI</h1>
                <p>Difficulty of the game can be configured with just a few parameters on agents even without the reconfiguration of an Agent's HP or skills</p>
            </div>
            <div className='more-features-column-item'>
                <h1>Animation</h1>
                <p>Seamlessly integrate the system with the work of your designer. Eliot supports Mecanim as well as Legacy animations</p>
            </div>

            <div className='more-features-column-item'>
                <h1>Experiment-friendly</h1>
                <p>Copy and edit files of Skills and Behaviours without the need to worry about breaking anything</p>
            </div>
            <div className='more-features-column-item'>
                <h1>Audio</h1>
                <p>Just set desired audio clips for pretty much any of an Agent's actions</p>
            </div>
        </div>
    </div>
);
export default MoreFeatures;
