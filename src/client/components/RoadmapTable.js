import React from 'react';

const RoadmapTable = props => (
    <table>
        <thead>
        <tr>
            <th>Feature</th>
            <th>Description</th>
            <th>Progress</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Eliot Hub</td>
            <td>A place where you can find Behaviours that you need for your project along with Skills that are used by Behaviours</td>
            <td>Planning</td>
        </tr>

        <tr>
            <td>Copy & Paste Parts of Behaviours</td>
            <td>Enable the User to copy selected nodes in the Behaviour Editor to duplicate them or paste them into another Behaviour</td>
            <td>Planning</td>
        </tr>
        <tr>
            <td>Library to be Extended</td>
            <td>More functions will be added to Eliot's components which will be accessible from the Behaviour Editor (e.g. jumping, crouching etc.)</td>
            <td>Planning</td>
        </tr>
        </tbody>

    </table>
);
export default RoadmapTable;