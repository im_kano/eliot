import React from 'react'

export const featuresArr = [
    {
        name: 'VISUAL PROGRAMMING',
        descr: 'Build complex behaviours within minutes.'
    },
    {
        name: 'PERCEPTION',
        descr: 'Agents use rays to understand the world around them.',
    },
    {
        name: 'MOTION',
        descr: 'Agents can move around using a system built with Unity\'s NavMesh.'
    },
    {
        name: 'INVENTORY',
        descr: 'Agents can pickup, use, wield and drop items.'
    },
    {
        name: 'RESOURCES',
        descr: 'Agents live, die, use energy for actions. Just tell them how.'
    },
    {
        name: 'SKILLS',
        descr: 'Attack, heal, cast spells... you name it. Each skill is represented by a separate file.',
    },
    {
        name: 'WAYPOINTS',
        descr: 'Spawn. Pool. Follow path. Define territories.'
    },
    {
        name: 'OPTIMIZATION',
        descr: 'Eliot can handle more than you probably need.'
    },
];

const FeaturesList = props => (
    <div className='content-pane-section-2-gifs-app-feature-list'>
        <h1>features</h1>
        <div className='content-pane-section-2-gifs-app-feature-list-uls'>
            <ul>
                {featuresArr.map((feature, index) => <li
                    className={props.activeFeature === feature.name ? 'selected' : ''} key={index}
                    onClick={() => props.loadSingleGif(feature)}>{feature.name}</li>)}
            </ul>
        </div>
    </div>
);
export default FeaturesList;