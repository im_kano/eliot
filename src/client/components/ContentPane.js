import React from 'react'

import AiIco from '-!svg-react-loader?name=AiIco!./../../../static/img/ai.svg';

import FeatureList from "./FeaturesList";
import RoadmapTable from "./RoadmapTable";
import MoreFeatures from "./MoreFeatures";

import LazyLoadGif from '../containers/LazyLoadGif';
// import FeatureGifDisplay from "./FeatureGifDisplay";
// import FeatureGifDescription from './FeatureGifDescription';
import '../scss/ContentPane.scss';

const ContentPane = props => (
    <div className='content-pane'>
        <div className='content-pane-title'><h1>ai framework for unity</h1></div>
        <div className='scroll' id='scroll'>
            <div className='content-pane-section content-pane-section-1' id='content-pane-one'>
                <div className='content-pane-section-1-img'>
                    <AiIco />
                </div>
                <div className="content-pane-section-1-text">
                    <p>Create high quality AI in Unity in a matter of minutes with the help of visual programming and a library of algorithms that are ready to provide your Agents with almost any type of interaction you might want in your game.</p>
                </div>
            </div>

            <div className='content-pane-section content-pane-section-2' id='content-pane-two'>
                <div className='content-pane-section-2-gifs-app'>
                    <FeatureList activeFeature={props.activeFeature} loadSingleGif = {props.loadSingleGif}/>
                    <div className='content-pane-section-2-gifs-app-feature-show'>
                        <div className='content-pane-section-2-gifs-app-feature-show-img'>
                            <LazyLoadGif url={`/static/gif/${props.featureObj.name.replace(/\s/g, '')}.gif`} />
                        </div>
                        <div className='content-pane-section-2-gifs-app-feature-show-descr'>
                            <h1>{props.featureObj.descr}</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div className='content-pane-section content-pane-section-3' id='content-pane-four'>
                <MoreFeatures/>

                <div className='table-title'>
                    <h1>roadmap</h1>
                </div>
                <div className='table-content'>
                    <RoadmapTable />
                </div>
            </div>

        </div>
    </div>
);

export default ContentPane
