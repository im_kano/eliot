import React from 'react';

import FacebookIcon from '-!svg-react-loader?name=FacebookIcon!./../../../static/img/facebook.svg';
import LinekedIn from '-!svg-react-loader?name=LinkedIn!./../../../static/img/linkedin.svg';
import InstagramIcon from '-!svg-react-loader?name=InstagramIcon!./../../../static/img/instagram.svg'
import UserAvatarPlaceholder from '-!svg-react-loader?name=UserAvatarPlaceholder!./../../../static/img/user.svg';
import TwitterIcon from '-!svg-react-loader?name=TwitterIcon!./../../../static/img/twitter-social-outlined-logo.svg';

import '../scss/RightPane.scss';

import {NavLink} from 'react-router-dom';

const RightPane = props => (
    <div className='right-pane' id='right-pane-id'>
        <div
            className="right-pane-user-avatar"
            onClick={props.showRegLogHandler}
        >
            <UserAvatarPlaceholder />
        </div>
        <div className="right-pane-menu">
            <ul>
                <li>
                    <NavLink exact to='/' activeClassName="selected">home</NavLink>
                </li>
                <li>
                    <NavLink to='/learn' activeClassName="selected">learn</NavLink>
                </li>
                <li>
                    <NavLink to='/contacts' activeClassName="selected">contacts</NavLink>
                </li>
            </ul>
        </div>
        <div className="right-pane-social">
            <a href="_#">
                <FacebookIcon/>
            </a>
            <a href="_#" id="insta">
                <InstagramIcon/>
            </a>
            <a href="_#">
                <LinekedIn/>
            </a>
            <a href="_#">
                <TwitterIcon/>
            </a>
        </div>
    </div>
);

export default RightPane;
