import React from 'react';
import {NavLink} from 'react-router-dom';

import '../scss/LeftPane.scss';

const LeftPane = ({showModal, showModalHandler}) => {
    let showhide = showModal ? "btn-5 wants" : 'btn-5';
    return(
    <div className='left-pane' id='left-pane-id'>
        <div className='eliot-logo'>
            <NavLink  to='/' activeClassName="home-selected"> Eliot </NavLink>
        </div>
        <div className='left-pane-arrow'>
            <div className='arrow'/>
        </div>
        <div className='buy-button'>
            <button className={showhide} onClick={showModalHandler}> BUY </button>
        </div>
    </div>
    );
};

export default LeftPane;
